## Register Python model assets to Model Manager
# reminder to remove hard coded passwords
    # these should be set as environment variables

import sys
import os
from viyapy import ViyaClient
from viyapy.variables import String, Decimal, Boolean, Date, DateTime, Integer

import logging
logger = logging.getLogger('viyapy')
logger.addHandler(logging.StreamHandler(sys.stdout))
logger.setLevel(logging.INFO)

protocol=os.environ['PROTOCOL']
server = os.environ['SERVER']
user = os.environ['USER']
password = os.environ['PASSWORD']
dirpath = os.environ['WORKINGDIR']

client = ViyaClient(protocol + '://' + server)
token = client.logon.authenticate(user, password)

repositoryName = 'Repository 1'
repository = client.model_manager.get_repository(repositoryName)

project = repository.create_project(
    name='EYAP_Jenkins_Register_v1',
    description='Project demonstrating management of open source models in Viya',
    function='Classification',
    target_level='binary',
    target_event_value='1',
    class_target_values='1,0',
    target_variable='BAD',
    event_probability_variable='P_BAD1',
    external_url='http://github.com/user/project',
    input=[
        Decimal('BAD', level='binary'),
        Decimal('CLAGE'),
        Decimal('CLNO'),
        Decimal('DEBTINC'),
        Decimal('DEROG'),
        String('JOB'),
        Decimal('LOAN'),
        Decimal('MORTDUE'),
        Decimal('NINQ'),
        String('REASON'),
        Decimal('VALUE'),
        Integer('YOJ')
    ],
    output=[
        Decimal('P_BAD0', description='Probability of not defaulting'),
        Decimal('P_BAD1', description='Probability of defaulting')
    ])

model = project.create_model(
    name='Python_GradientBoost',
    algorithm='scikit-learn.GradientBoostingClassifier',
    modeler='Yi Jian Ching',
    files=[
        dirpath + 'gboost_train.py',
        dirpath + 'gboost_score.py',
        (dirpath + 'model/gboost_obj_3_6_1.pkl', True)
    ])

perf_task = project.create_performance_definition(
    name='Performance Monitoring',
    models=[model])

metadata = {
    'projectId': project.Id,
    'projectName': project.Name,
    'modelId': model.Id,
    'modelName': model.Name,
    'taskId': perf_task.Id,
    'contentId': '',
    'targetVariable': project.targetVariable,
    'targetEventValue': project.targetEventValue,
    'nonEventValue': project.classTargetValues.replace(project.targetEventValue+',',""),
    'inputVariables': ', '.join(perf_task.inputVariables),
    'outputVariables': ', '.join(perf_task.outputVariables),
    'modelDisplayName': model.Name,
    'dataPrefix': perf_task.dataPrefix,
    'sequence': '',
    'timeValue': ''
}

import csv
with open(os.getcwd() + "/model/gboost_metadata.csv",'w', newline='') as f:
    w = csv.writer(f)
    w.writerow(metadata.keys())
    w.writerow(metadata.values())

import sys
print(sys.version)

from sklearn.metrics  import confusion_matrix, accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import GradientBoostingClassifier
import collections
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import getpass
import requests
import json
import time
import os

# Define directory and data files
indata_dir = os.getcwd()+'/'
indata     = 'HMEQ'
df_data_card = pd.read_csv(indata_dir + indata + '.csv')

target       = df_data_card.columns[0]
class_inputs = list(df_data_card.select_dtypes(include=['object']).columns)

# impute data
df_data_card = df_data_card.fillna(df_data_card.median())
df_data_card['JOB'] = df_data_card.JOB.fillna('Other')

# dummy the categorical variables
df_data_card_ABT = pd.concat([df_data_card, pd.get_dummies(df_data_card[class_inputs])], axis = 1).drop(class_inputs, axis = 1)
df_all_inputs = df_data_card_ABT.drop(target, axis=1)

# partition data
from sklearn.model_selection import train_test_split
X_train, X_valid, y_train, y_valid = train_test_split(
     df_all_inputs, df_data_card_ABT[target], test_size=0.33, random_state=54321)

# Build sklearn Gradient Boost
gb = GradientBoostingClassifier(random_state=54321)
gb.fit(X_train, y_train)

import pickle
output = open(indata_dir + 'model/gboost_obj_3_6_1.pkl', 'wb')
pickle.dump(gb, output)
output.close()
